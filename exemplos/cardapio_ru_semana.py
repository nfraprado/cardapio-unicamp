#!/bin/python

# Mostra qualidade das refeições para o restante da semana com base nos pratos.
# Refeições normais são indicadas por '0', boas por '+' e ruins por '-'.
# Os dias são separados por '|', e para cada dia o primeiro símbolo indica o
# almoço e o segundo a janta.
# A refeição é considerada ruim se contém alguma palavra da lista 'ruim', caso
# contrário é considerada boa se contém alguma palavra da lista 'bom'.
# Se não for bom nem ruim então é normal.
# Exemplo de saída executando em uma quarta-feira, em que os almoços são bons e
# as jantas são ruins:
# +-|+-|+-

import os
import json
import datetime as dt

ruim = ['peixe', 'moída', 'misto', 'isca', 'picada']
bom = ['pernil', 'strogonoff', 'feijoada', 'steak', 'nuggets', 'filezinho']

semana = ""

dia_semana = dt.date.today().weekday()+2
if dia_semana > 6:
    dia_semana = 2

for i in range(dia_semana, 7):
    cmd = f"cardapio-unicamp --json --dia {i}a"
    menu_json = os.popen(cmd).read()
    try:
        menu_json = json.loads(menu_json)
    except:
        semana += "__|"
        continue

    almoco = menu_json['almoco']['principal'][0].lower()
    jantar = menu_json['jantar']['principal'][0].lower()

    for prato in [almoco, jantar]:
        if any(r in prato for r in ruim):
            semana += "-"
        elif any(b in prato for b in bom):
            semana += "+"
        else:
            semana += "0"

    semana += "|"

semana = semana.strip('|')
print(semana)
